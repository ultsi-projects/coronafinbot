import { roundToTwoDecimals } from '../utils';

type Case = {
  id: string;
  date: string;
  healthCareDistrict: string;
};

export type ConfirmedCase = Case & {
  infectionSourceCountry: string | null;
  infectionSource: number | string;
};

export type DeathCase = Case;
export type RecoveredCase = Case;

export type HSCoronaResult = {
  confirmed: ConfirmedCase[];
  deaths: DeathCase[];
  recovered: RecoveredCase[];
};

export type CasesByDate = {
  date: string;
  cases: Case[];
};

type StatsForDate = {
  growth: number;
  total: number;
  cumulativeTotal: number;
  date: string;
};

export const getCasesByDates = (cases: Case[]): CasesByDate[] => {
  return cases
    .sort((a, b) => (a.date < b.date ? -1 : 1))
    .reduce((acc: CasesByDate[], cur: Case) => {
      const date = cur.date.split('T')[0];
      if (acc.length == 0) {
        acc.push({ date, cases: [cur] });
      } else if (acc[acc.length - 1].date == date) {
        acc[acc.length - 1].cases.push(cur);
      } else if (acc[acc.length - 1].date !== date) {
        acc.push({ date, cases: [cur] });
      } else {
        console.error('UNEXPECTED CONDITION.');
      }
      return acc;
    }, []);
};

export const getStatsForDates = (casesByDates: CasesByDate[]) =>
  casesByDates.reduce((acc: StatsForDate[], cur: CasesByDate, index: number) => {
    const prevCumulativeTotal = index !== 0 ? acc[index - 1].cumulativeTotal : 0;
    return [
      ...acc,
      {
        total: cur.cases.length,
        cumulativeTotal: prevCumulativeTotal + cur.cases.length,
        growth: (prevCumulativeTotal + cur.cases.length) / prevCumulativeTotal - 1,
        date: cur.date,
      },
    ];
  }, []);
