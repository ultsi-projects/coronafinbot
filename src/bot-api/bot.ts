import TelegramBot from 'node-telegram-bot-api';

const TOKEN = process.env.BOT_TOKEN || '';
const bot = new TelegramBot(TOKEN, { polling: true });
const chatId = '-1001392254204';

export const sendMsg = (text: string): Promise<void | TelegramBot.Message> => {
  return bot
    .sendMessage(chatId, text)
    .catch((err: any) => console.error(`failed to send bot message: ${err}`));
};
