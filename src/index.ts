import { sendMsg } from './bot-api/bot';
import fetch from 'node-fetch';
import { readCounter, writeCounter } from './counter';
import {
  HSCoronaResult,
  getCasesByDates,
  getStatsForDates,
  ConfirmedCase,
  DeathCase,
} from './corona-lib';

const LOOP_INTERVAL = 60 * 1000;

const onlyUnique = (value: any, index: number, self: any[]): boolean => {
  return self.indexOf(value) === index;
};

const formConfirmedCasesText = (confirmedCases: ConfirmedCase[]) => {
  if (!confirmedCases.length) return `No new confirmed cases.`;

  const districts = confirmedCases
    .map(c => c.healthCareDistrict)
    .filter(onlyUnique)
    .join(', ');

  return `New confirmed cases: ${confirmedCases.length}.\nIn healthcare district(s): ${districts}.`;
};

const formDeathCasesText = (deathCases: DeathCase[]) => {
  if (!deathCases.length) return 'No new deaths.';

  const districts = deathCases
    .map(c => c.healthCareDistrict)
    .filter(onlyUnique)
    .join(', ');

  return `New deaths: ${deathCases.length}.\nIn healthcare district(s): ${districts}.`;
};

const formTotalsText = (json: HSCoronaResult) => {
  const casesByDates = getCasesByDates(json.confirmed);
  const statsByDates = getStatsForDates(casesByDates).map(({ growth, cumulativeTotal }) => ({
    cumulativeTotal,
    growth: (growth * 100).toPrecision(2),
  }));
  const mortalityPercent = ((json.deaths.length / json.confirmed.length) * 100).toPrecision(2);
  return (
    `Confirmed cases for the last 3 days: ${
      statsByDates[statsByDates.length - 3].cumulativeTotal
    } (+${statsByDates[statsByDates.length - 3].growth}%),  ${
      statsByDates[statsByDates.length - 2].cumulativeTotal
    } (+${statsByDates[statsByDates.length - 2].growth}%), ${
      statsByDates[statsByDates.length - 1].cumulativeTotal
    } (+${statsByDates[statsByDates.length - 1].growth}%).` +
    `\nConfirmed: ${json.confirmed.length}.` +
    `\nDeaths: ${json.deaths.length} (${mortalityPercent}%).`
  );
};

/* eslint-disable @typescript-eslint/explicit-function-return-type */
const mainLoop = async () => {
  try {
    const res = await fetch(
      'https://w3qa5ydb4l.execute-api.eu-west-1.amazonaws.com/prod/finnishCoronaData/v2',
    );
    const json: HSCoronaResult = await res.json();
    const newConfirmedCount = json.confirmed.length;
    const newDeathCount = json.deaths.length;
    const newRecoveredCount = json.recovered.length;
    const {
      confirmedCount: oldConfirmedCount,
      deathCount: oldDeathCount,
      recoveredCount: oldRecoveredCount,
    } = await readCounter();
    console.log(
      `Data fetch results: confirmed ${newConfirmedCount}/${oldConfirmedCount}, deaths ${newDeathCount}/${oldDeathCount}, recovered: ${newRecoveredCount}/${oldRecoveredCount}`,
    );
    if (newConfirmedCount > oldConfirmedCount || newDeathCount > oldDeathCount) {
      const newConfirmedCases = json.confirmed.slice(oldConfirmedCount, newConfirmedCount);
      const newDeathCases = json.deaths.slice(oldDeathCount, newDeathCount);
      const confirmedText = formConfirmedCasesText(newConfirmedCases);
      const deathsText = formDeathCasesText(newDeathCases);
      const totalsText = formTotalsText(json);

      const msg = `New data:\n\n${confirmedText}\n\n${deathsText}\n\n${totalsText}`;

      await sendMsg(msg);
      await writeCounter(newConfirmedCount, newDeathCount, newRecoveredCount);
    }
  } catch (err) {
    console.error(err);
  }
  setTimeout(mainLoop, LOOP_INTERVAL);
};

mainLoop();
