import { readFile, writeFile } from 'fs';

export const readCounter = async (): Promise<{
  confirmedCount: number;
  deathCount: number;
  recoveredCount: number;
}> => {
  return new Promise((resolve, reject) => {
    readFile('data/.corona_index_counter', (err, contents) => {
      if (err) {
        reject(err);
      } else {
        const numbers = contents.toString().split(';');
        resolve({
          confirmedCount: Number(numbers[0]),
          deathCount: Number(numbers[1]),
          recoveredCount: Number(numbers[2]),
        });
      }
    });
  });
};

export const writeCounter = async (
  confirmedCount: number,
  deathCount: number,
  recoveredCount: number,
): Promise<void> => {
  return new Promise((resolve, reject) => {
    writeFile(
      'data/.corona_index_counter',
      `${confirmedCount};${deathCount};${recoveredCount}`,
      err => {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      },
    );
  });
};
