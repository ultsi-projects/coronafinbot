FROM node:13.10.1

WORKDIR /app/
COPY package.json /app/
RUN npm install
RUN npm install typescript ts-node -g

RUN mkdir -p /app/src/

COPY src/ src/
COPY tsconfig.json .
RUN tsc -p .
COPY data/ data/
RUN echo "Europe/Helsinki" > /etc/timezone
RUN dpkg-reconfigure -f noninteractive tzdata

CMD ["npm", "run", "start_prod"]
